
    function setupButton(ID, functionName) {
      const btn = document.getElementById(ID);
      btn.addEventListener("click", functionName);
    }

    function enactAnnoy() {
      let i = 10;
      while (i > 0) {
        if (i === 3) {
          alert("Are you sure you aren't?");
        } else if (i === 2) {
          alert("Are you REALLY sure?")
        } else {
          alert("Are you annoyed?");
        }
        i--;
      }
    }

    function add7() {
      let num = prompt("What number do you want to add to 7?", "");
      num = parseInt(num);
      alert(num + 7);
    }

    function multiplyNum() {
      let num1 = prompt("Enter your first number to multiply.", "14023");
      let num2 = prompt("Enter the second number to multiply.", "3");
      num1 = parseInt(num1);
      num2=parseInt(num2);
      alert(num1 * num2);
    }

    function capitalizeWord() {
      let originalString = prompt("Enter a word to capitalize.", "wIgGlE");
      let lowerString = originalString.toLowerCase();
      let capString = lowerString.charAt(0).toUpperCase() + lowerString.slice(1);
      alert(capString);
    }

    function showLastLetter() {
      let originalString = prompt("What word do you want to see the last letter of?", "Matterhorn");
      let lastLetter = originalString.slice(-1);
      alert(`The last letter of "${originalString}" is the letter "${lastLetter}".`);
    }

    function random100() {
      let result= Math.floor(Math.random() * 100) + 1;
      alert(`Here is a random number between 1 and 100: ${result}.`);
    }

    function fizzBuzz() {
      let num = prompt("Please enter your number. (Check the console for output)", "");
      num = parseInt(num); // yes, I know I can combine this with the above, but I prefer it this way

      for (let i =1; i <= num; i++) {
        if (i % 3 === 0 && i % 5 === 0) {
          console.log("FizzBuzz")
        } else if (i % 3 === 0) {
          console.log("Fizz");
        } else if (i % 5 === 0) {
          console.log("Buzz")
        } else {
          console.log(i);
        }
        i++;
      }
    }

    // rock, paper, scissors
    function getComputerChoice() {

      let choice = Math.floor(Math.random() * 3) + 1;

      if (choice === 1) {
        choice = "rock";
      } else if (choice === 2) {
        choice = "paper";
      } else {
        choice = "scissors";
      }
      return choice;
    }

    function playRound(playerSelection, computerSelection) {

      switch(playerSelection) {
        case "rock":
          if (computerSelection === "rock") {
            return "You tied!";
          } else if (computerSelection === "paper") {
            return "You lost!";
          } else {
            return "You won!";
          }
          break;
        case "paper":
          if (computerSelection === "rock") {
            return "You won!";
          } else if (computerSelection === "paper") {
            return "You tied!";
          } else {
            return "You lost!";
          }
          break;
        case "scissors":
          if (computerSelection === "rock") {
            return "You lost!";
          } else if (computerSelection === "paper") {
            return "You won!";
          } else {
            return "You tied!";
          }
          break;
        default:
          return "Sorry something went wrong.";
      }
    }

    function game() {

        const playerSelection = prompt("Rock, Paper, or Scissors?").toLowerCase();
        alert(`You chose ${playerSelection}.`);
        const computerSelection = getComputerChoice();
        alert(`Your opponent chose ${computerSelection}.`);
        alert(playRound(playerSelection, computerSelection));
    }

    //diceRoller
    function genRandom(range) {
      let choice = Math.floor(Math.random() * range) + 1;
      return choice;
    }

    function rollDice() {
      let numDice = parseInt(prompt("How many dice?"));
      let diceSides = parseInt(prompt("How many sides do the dice have each (same number for all dice)?"))
      
      const rolls = [];

      for (let i = 0; i < numDice; i++) {
        let result = genRandom(diceSides);
        rolls[i] = result;
      }

      alert("You rolled " + numDice + " dice with " + diceSides + " sides. The rolls were: " + rolls.join(", ") + ".");
    }

    //makeDecision
    function makeDecision() {
      const choices = [];

      let numOptions = parseInt(prompt("I will help you with your decision. How many choices are there?"));
      
      for (let i = 0; i < numOptions; i++) {
        let choice = prompt("Enter your choices one at a time. Enter here:");
        choices[i] = choice;
      }
      
      let finalChoice = Math.floor(Math.random() * choices.length);
      alert("I choose " + choices[finalChoice]);

    }

    //loremIpsum
    const loremIpsum = [
      
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vitae tincidunt lorem. In porta velit tincidunt, tempus velit a, pharetra massa. Suspendisse augue ex, iaculis congue sodales sit amet, iaculis quis arcu. Donec viverra arcu vel ipsum facilisis ultricies. Proin euismod sagittis justo ut aliquet. Duis ante ipsum, interdum quis nisl vel, luctus tempor tellus. Donec interdum, nibh sit amet feugiat ultricies, quam quam ornare orci, a posuere turpis augue eu urna.",
      
      "Ut eget pulvinar odio. Cras in ligula odio. In ac nisl quis massa tincidunt imperdiet. Cras gravida est nec lobortis gravida. In ac velit pellentesque, blandit lorem ac, convallis libero. Aenean gravida nulla eu pellentesque laoreet. Mauris gravida dolor urna, sit amet sagittis tellus dignissim ac. Proin bibendum quam magna, non pharetra urna sollicitudin sit amet. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Suspendisse potenti. Nullam ultricies ipsum eu ante porttitor dignissim. Praesent volutpat pellentesque luctus. Phasellus lacinia elit turpis, ut molestie nisi varius eget.",
      
      "Ut fringilla, sapien sit amet iaculis rutrum, orci urna aliquam velit, vel blandit enim dolor eget lorem. Ut aliquet sapien lorem, ultricies rhoncus augue auctor eget. Etiam vel mauris quis nibh consectetur auctor vel blandit enim. Ut lacinia malesuada metus a ultricies. Fusce tempus at lorem semper posuere. Nunc pharetra, nisi id ultricies eleifend, ipsum sem interdum ligula, sit amet blandit enim nibh at libero. Nunc sed dapibus leo.",
      
      "Suspendisse sit amet dolor lectus. Quisque ligula felis, porta sit amet purus rhoncus, lobortis facilisis erat. Sed vulputate porta placerat. Sed vel pretium dolor. Praesent pulvinar eros vitae arcu condimentum pretium. Vivamus eu est at tortor venenatis auctor sodales a elit. Aenean sodales sem id mi ultrices, at aliquam eros maximus.",
      
      "Pellentesque non consectetur nisi, eu consequat nulla. Duis sed hendrerit ipsum. Vivamus malesuada ipsum eget interdum dictum. Integer eu elit volutpat, lacinia neque et, cursus tortor. Etiam euismod consectetur dui non luctus. Aliquam erat volutpat. Maecenas quis quam lorem. Proin mi ligula, tristique id pellentesque sed, pharetra vel lacus. Aliquam vel pharetra nisl, sed pellentesque massa. Nulla aliquet massa et feugiat mattis.",
      
      "Cras viverra pulvinar mollis. Curabitur commodo dui quis enim elementum vulputate. Suspendisse potenti. Fusce justo turpis, porta eget lobortis sit amet, sagittis id risus. Phasellus eget blandit purus. Ut gravida semper est, non tempus libero viverra et. Aliquam sit amet tellus sed tortor congue scelerisque. Mauris vel urna tincidunt, ultrices justo ut, sagittis ante. Sed lobortis, ante luctus hendrerit ultrices, nisi metus viverra diam, id volutpat lectus mauris quis nulla. Sed fringilla nulla eu blandit eleifend. Aenean porttitor imperdiet faucibus. Donec efficitur non magna ac ullamcorper. Proin maximus sit amet urna quis placerat. Praesent dolor velit, tincidunt pulvinar consequat non, fermentum non enim. Pellentesque et mi ullamcorper, dignissim nisi vel, gravida ipsum.",
      
      "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi at ante sed massa elementum dignissim at at ex. Nullam commodo nisl ac nunc vulputate convallis. Quisque sagittis dapibus purus eu hendrerit. Morbi euismod arcu quis metus tincidunt porttitor. Nulla ullamcorper et erat id tristique. Aliquam finibus metus eget lacus fringilla accumsan. Duis auctor lacus eget mauris congue rhoncus. Etiam ultrices nisi a tincidunt bibendum. Vestibulum tincidunt tellus purus, a scelerisque tellus placerat eu. Nulla lobortis dolor ornare vestibulum interdum. Nunc ultrices erat quis blandit volutpat. Integer porta mattis ornare. Aenean luctus lectus ac diam dapibus, id varius nulla mattis.",
      
      "Integer eget lacinia dui, eget faucibus velit. Sed rutrum nisl sed ex tincidunt convallis. Integer mattis malesuada dolor, eu molestie mi cursus pellentesque. Integer eget nunc ante. Cras rhoncus tellus lacus, eu porttitor lorem porta non. Morbi eget purus a augue dignissim congue. Phasellus volutpat ligula id odio vehicula, eu venenatis turpis dapibus. Integer id suscipit ex. Donec convallis tempor velit, nec cursus sem commodo lobortis.",
      
      "Donec euismod est at venenatis efficitur. Curabitur volutpat dui eu ipsum ultricies congue. Curabitur condimentum, est nec euismod euismod, ante mauris rhoncus tellus, quis hendrerit quam metus ut ante. Mauris maximus massa id odio posuere efficitur. Cras ut suscipit erat. Integer augue arcu, mollis quis blandit id, sagittis at quam. In sit amet neque sem. Vestibulum risus sem, rutrum a auctor sit amet, ultricies sit amet sem. Ut facilisis ex velit, vel suscipit velit rutrum non. Ut sed nisi facilisis, fermentum dolor at, molestie dolor. Sed magna diam, iaculis vitae ipsum pretium, commodo rhoncus est. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aliquam et nisl erat. Nullam id augue nec ex porta bibendum ac molestie ex. Nullam facilisis volutpat risus non suscipit. Integer at pulvinar sem.",
      
      "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Donec facilisis varius pulvinar. Mauris et nisi vitae lacus interdum pharetra auctor vel odio. Donec enim erat, viverra sed placerat ac, ultrices eget tellus. Etiam gravida, libero in viverra eleifend, tellus est molestie justo, ut laoreet eros lorem non est. Integer dignissim augue a dolor hendrerit rhoncus. Vivamus sed rhoncus est. Duis vel ex lobortis, dapibus ligula non, sollicitudin dui. Nullam dignissim consectetur dolor, at molestie odio lacinia vitae. Aenean auctor massa in urna ullamcorper ullamcorper. Quisque nunc dolor, hendrerit ut metus in, blandit bibendum elit. Curabitur sagittis ipsum dolor, quis interdum ipsum blandit vel. Vivamus risus ex, mollis sit amet nisl sit amet, suscipit consequat ex. Proin id felis at dui lacinia placerat at semper urna. Sed nec tincidunt est. Nunc tempor diam lacus, in venenatis arcu aliquet quis.",
      
      "Aenean sed arcu congue, consectetur eros id, venenatis dui. Nunc eget nunc maximus, placerat enim nec, aliquam justo. Sed tristique interdum ipsum, ac congue nisl maximus vitae. Nulla eleifend maximus efficitur. Nulla pharetra facilisis dapibus. Suspendisse sollicitudin a nunc eu ornare. Curabitur rhoncus orci ut augue placerat consectetur. Phasellus libero turpis, bibendum in turpis sit amet, commodo malesuada justo. Sed et molestie erat. Curabitur posuere condimentum libero, nec ultrices lectus consequat id. Donec ac tempor magna, sed tincidunt purus. Praesent molestie porttitor augue. Proin tincidunt lacinia interdum. Phasellus iaculis ornare tristique. Aliquam odio odio, feugiat eget odio in, dapibus aliquam magna.",
      
      "Aliquam congue vel turpis at maximus. Morbi ut augue a sem interdum cursus non et purus. Phasellus dignissim sapien neque. Vivamus venenatis luctus ultricies. Donec blandit, neque eu tempus fermentum, lorem turpis consectetur ex, elementum viverra velit augue ac mauris. Praesent maximus eu mauris quis condimentum. Nam pharetra ex ut cursus aliquet.",
      
      "Donec vitae scelerisque libero. Ut laoreet, nisi vitae tincidunt ullamcorper, dui libero tempus neque, ut mattis ante massa non purus. Proin vitae interdum ligula. Curabitur convallis viverra risus. Integer at eleifend ante. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed in rhoncus enim. Mauris mauris mauris, ullamcorper vel semper at, finibus ut mauris. Fusce dolor justo, posuere at tempus quis, vehicula sed justo. Curabitur fringilla orci orci. Phasellus id vehicula lectus. In rutrum placerat porta. Aliquam convallis congue leo ac tempus. Etiam malesuada tellus quis ante mollis commodo. Mauris lacinia nec tellus a lacinia. Vivamus consequat sapien augue, id mollis augue consequat eu.",
      
      "Quisque sed viverra erat, maximus sollicitudin augue. Nam sollicitudin ullamcorper euismod. Pellentesque dignissim vehicula ultrices. Fusce vitae ultrices libero, eget condimentum elit. Sed at tortor vel sem rhoncus feugiat non et lacus. Praesent vel sapien at nulla pretium tincidunt non et dolor. Integer tincidunt ullamcorper vulputate. Sed ullamcorper, massa quis pulvinar pulvinar, sem quam feugiat metus, et hendrerit tortor diam pharetra tellus. Ut sem eros, tincidunt id cursus non, varius sit amet risus. Proin dignissim elit id libero placerat ultricies. Sed gravida nisi ac nibh interdum, sed mollis metus molestie. Ut nec neque dignissim, dictum odio non, fringilla lorem. Duis metus enim, commodo eget porta non, suscipit non ipsum. Ut pulvinar justo non velit posuere, non posuere lorem aliquam. Sed mattis varius vulputate. Sed vel maximus tellus.",
      
      "Cras venenatis vitae ipsum non semper. Etiam nulla purus, fringilla et scelerisque in, dignissim nec dolor. Aliquam non porta metus, non aliquam dui. Suspendisse potenti. Ut metus risus, convallis a sagittis efficitur, molestie eget tellus. Morbi ullamcorper tincidunt libero, ut mattis justo feugiat et. Duis id metus non sapien sollicitudin iaculis. Etiam justo dolor, laoreet in vehicula in, hendrerit eget lectus. Fusce a augue eget elit faucibus fringilla id sed mauris. Duis varius mauris nec nunc consequat imperdiet. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus."
        
      ];
      
      function generateLorem() {
        let numParagraphs = parseInt(prompt("How many paragraphs of Lorem Ipsum would you like?"));
        let paragraphArray = [];
      
        for (let i = 0; i < numParagraphs; i++) {
          let paragraphChoice = loremIpsum[genRandom(loremIpsum.length)];
          paragraphArray[i] = paragraphChoice;
        }
      
        alert(paragraphArray.join("\r\n"));
      }

      //Temp Converter
      const ftoc = function (temp) {
        return Math.round(((temp - 32) * 5 / 9) * 10) / 10;
      };
      
      const ctof = function (temp) {
        return Math.round((temp * 9 / 5 + 32) * 10) / 10;
      };

      function tempConvert() {
        let unit = prompt("What unit are you converting from (f or c)?").toLowerCase();
        
        if (unit === "f" || unit === "fahrenheit") {
          let temp = prompt("How many degrees Fahrenheit?");
          alert(temp + " degrees Fahrenheit is " + ftoc(temp) + " degrees Celcius");
        } else if (unit === "c" || unit === "celsius") {
          let temp = prompt("How many degrees Celsius?");
          alert(temp + " degrees Celsius is " + ctof(temp) + " degrees Fahrenheit.");
        } else {
          alert("Sorry I did not understand. Let's try again.");
          tempConvert();
        }
      }

      //Test add div before buttons for future output
      function testAdd() {
        const container = document.getElementById('container'); 
        
        const testArea = document.createElement('p');
        testArea.setAttribute('id', 'testArea');
        testArea.textContent = 'This is a test!';

        container.appendChild(testArea);
      }

      //Clear the `div` container
      function clearDiv() {
        let div = document.getElementById('container');
        while(div.firstChild) {
          div.removeChild(div.firstChild);
        }
      }